//
//  GameViewController.swift
//  p08-agdere
//
//  Created by Aris Agdere on 4/24/16.
//  Copyright © 2016 aagdere1. All rights reserved.
//

import UIKit
import SpriteKit

class GameViewController: UIViewController{
    
    override func viewDidLoad() {
        self.view.backgroundColor = UIColor.redColor()
        if let scene = GameScene(fileNamed:"GameScene") {
            // Configure the view.
            
            let skView = SKView(frame: self.view.frame)
            self.view.addSubview(skView)
            //let scene = GameScene(size: skView.bounds.size)
            skView.showsFPS = true
            skView.showsNodeCount = true
            skView.ignoresSiblingOrder = true
            scene.scaleMode = .AspectFill
            //let skView = self.view as! SKView
            skView.showsFPS = true
            skView.showsNodeCount = true
            
            /* Sprite Kit applies additional optimizations to improve rendering performance */
            skView.ignoresSiblingOrder = true
            
            /* Set the scale mode to scale to fit the window */
            scene.scaleMode = .AspectFill
            skView.presentScene(scene)
        }
    }
    
//    override func viewDidAppear(animated: Bool) {
//        if let scene = GameScene(fileNamed:"GameScene") {
//            // Configure the view.
//            
//            let skView = SKView(frame: self.view.frame)
//            self.view.addSubview(skView)
//            //let scene = GameScene(size: skView.bounds.size)
//            skView.showsFPS = true
//            skView.showsNodeCount = true
//            skView.ignoresSiblingOrder = true
//            scene.scaleMode = .AspectFill
//            //let skView = self.view as! SKView
//            skView.showsFPS = true
//            skView.showsNodeCount = true
//            
//            /* Sprite Kit applies additional optimizations to improve rendering performance */
//            skView.ignoresSiblingOrder = true
//            
//            /* Set the scale mode to scale to fit the window */
//            scene.scaleMode = .AspectFill
//            skView.presentScene(scene)
//        }
//    }
    
}
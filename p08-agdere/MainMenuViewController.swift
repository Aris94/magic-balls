//
//  MainMenuViewController.swift
//  p08-agdere
//
//  Created by Aris Agdere on 4/23/16.
//  Copyright © 2016 aagdere1. All rights reserved.
//

import UIKit
import SpriteKit
import AVFoundation

class MainMenuViewController: UIViewController {
    
    var screenWidth:CGFloat = 0.0
    var screenHeight:CGFloat = 0.0
    var startButton:UIImage = UIImage(named: "StartButton.png")!
    var optionsButton:UIImage = UIImage(named: "OptionsButton.png")!
    var gameViewController = GameViewController()
    var audioPlayer:AVAudioPlayer!

    override func viewDidLoad() {
        super.viewDidLoad()
        assignGlobalVariables()
        setupBackground()
        setupButtons()
    }
    
    func assignGlobalVariables(){
        screenWidth = UIScreen.mainScreen().bounds.width
        screenHeight = UIScreen.mainScreen().bounds.height
    }
    
    func setupButtons(){
        setupButton(startButton, position: CGPoint(x: screenWidth * 0.07, y: screenHeight * 0.40), title: "start")
        setupButton(optionsButton, position: CGPoint(x: screenWidth * 0.07, y: screenHeight * 0.50), title: "options")
    }
    
    func setupButton(image:UIImage, position:CGPoint, title:String){
        let size = CGSize(width: screenWidth * 0.4, height: screenHeight * 0.05)
        let imageView = UIImageView(image: image)
        imageView.frame = CGRect(origin: position, size: size)
        let button = UIButton(frame: imageView.frame)
        button.frame.origin = position
        button.addTarget(self, action: #selector(MainMenuViewController.buttonAction(_:)), forControlEvents: UIControlEvents.TouchUpInside)
        button.setTitle(title, forState: UIControlState.Normal)
        self.view.addSubview(button)
        self.view.addSubview(imageView)
    }
    
    func buttonAction(sender: UIButton){
        playSound("levelCompleted")
        if(sender.currentTitle == "start"){
            loadGame()
        }
        if(sender.currentTitle == "options"){
            //Options Menu
        }
    }
    
    func setupBackground(){
        self.view.backgroundColor = UIColor.lightGrayColor()
        let backgroundImage = UIImage(named: "LaunchScreen.jpg")
        let backgroundImageView = UIImageView(image: backgroundImage)
        backgroundImageView.frame = CGRect(x: 0, y: 0, width: screenWidth, height: screenHeight)
        self.view.addSubview(backgroundImageView)
    }
    
    func loadGame(){
        //self.showViewController(levelViewController, sender: self)
        self.showViewController(gameViewController, sender: self)
    }
    
    func playSound(name:String) {
        let url:NSURL = NSBundle.mainBundle().URLForResource(name, withExtension: "mp3")!
        
        do { audioPlayer = try AVAudioPlayer(contentsOfURL: url, fileTypeHint: nil) }
        catch let error as NSError { print(error.description) }
        
        audioPlayer.numberOfLoops = 0
        audioPlayer.prepareToPlay()
        audioPlayer.play()
        
    }

}
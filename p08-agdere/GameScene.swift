//
//  GameScene.swift
//  p08-agdere
//
//  Created by Aris Agdere on 4/14/16.
//  Copyright (c) 2016 aagdere1. All rights reserved.
//

import SpriteKit
import AVFoundation

class GameScene: SKScene, SKPhysicsContactDelegate {
    
    struct PhysicsCategory {
        static let ball:UInt32        = 0b000001
        static let magnet:UInt32      = 0b000010
        static let wall:UInt32        = 0b000100
        static let goal:UInt32        = 0b001000
        static let portal:UInt32      = 0b010000
        static let magnetForce:UInt32 = 0b100000
    }
    
    enum Tool{
        case magnet
        case portal
        case line
    }
    
    var testing = false
    var level = 6
    var musicStartsPaused = false
    var maxWallLength:CGFloat = 0.0

    let titleLabel = SKLabelNode()
    var portal1 = SKSpriteNode()
    var portal2 = SKSpriteNode()
    var ballLabel = SKLabelNode()
    var pauseButton = SKLabelNode()
    var resetButton = SKLabelNode()
    var ball = SKShapeNode()
    var ballColor = UIColor.redColor()
    var originalBallColor = UIColor()
    var otherBallColor = UIColor()
    var goal = SKShapeNode()
    let title = "Magic Balls"
    var ballSpawnPosition = CGPoint()
    var ballRadius:CGFloat = 0.0
    var goalRadius:CGFloat = 0.0
    let labelFont = "Chalkduster"
    var screenWidth:CGFloat = 0.0
    var screenHeight:CGFloat = 0.0
    var numBalls = 0
    var lastWall = SKShapeNode()
    var lastWallPoints = [CGPoint]()
    var lastWallLength:CGFloat = 0.0
    var bounceNoise = 1
    var audioPlayer = AVAudioPlayer()
    var test = 0
    var portalNum = 1
    var portalLock = 0
    var magnetSoundLock = 0
    var totalLevels = 1
    var levelDictionary = [String: AnyObject]()
    var levelLabel = SKLabelNode()
    var wallIcon = SKLabelNode()
    var wallIconIndicator = SKLabelNode()
    var magnetIcon = SKSpriteNode(texture: SKTexture(), size: CGSize())
    var magnetIconIndicator = SKLabelNode()
    var portalIcon = SKSpriteNode(texture: SKTexture(), size: CGSize())
    var portalIconIndicator = SKLabelNode()
    var limit = 0
    var toolLimitLabel = SKLabelNode()
    var wallsAllowed = true
    var magnetsAllowed = false
    var portalsAllowed = false
    var currentTools = [SKNode]()
    
    override func didMoveToView(view: SKView) {
        /* Setup your scene here */
        assignGlobalVariables()
        setupScene()
        setupButtons()
        setupLabels()
        setupWalls()
        setupIcons()
//        spawnBall(ballColor,
//                  radius: ballRadius,
//                  xIn: ballSpawnPosition.x,
//                  yIn: ballSpawnPosition.y)
//        spawnGoal(goalRadius,
//                  xIn: CGRectGetMaxX(self.frame)-ballRadius,
//                  yIn: CGRectGetMidY(self.frame))
        playSongAVPlayer("backgroundMusic")
        createLevelFromNum(level)
    }
    
    func assignGlobalVariables(){
        //For variables that cannot be assigned until the view has loaded
        screenWidth = UIScreen.mainScreen().bounds.width
        screenHeight = UIScreen.mainScreen().bounds.height
        self.size.width = screenWidth
        self.size.height = screenHeight
        ballSpawnPosition = CGPoint(x: screenWidth * 0.2, y: screenHeight * 0.9)
        ballRadius = CGRectGetMaxX(self.frame)*0.05
        goalRadius = ballRadius
        maxWallLength = screenWidth * 100//screenWidth * 0.5
        originalBallColor = ballColor
        otherBallColor = UIColor.blueColor()
        
        if(testing == false){
            level = 1
            musicStartsPaused = true
            maxWallLength = screenWidth * 0.5
        }
    }
    
    func setupScene(){
        self.view?.showsFPS = false
        self.view?.showsNodeCount = false
        self.backgroundColor = UIColor.whiteColor()
        self.physicsWorld.gravity = CGVector(dx: 0.0, dy: -9.8)
        self.physicsWorld.contactDelegate = self
        self.name = "scene"
    }
    
    func setupButtons(){
        setupLabel(pauseButton, fontColorIn: UIColor.clearColor(), fontSizeIn: 20, name: "pauseMusic", textIn: "🔊", xIn: screenWidth * 0.95, yIn: screenHeight * 0.02)
        setupLabel(resetButton, fontColorIn: UIColor.greenColor(), fontSizeIn: 20, name: "reset", textIn: "♻️", xIn: screenWidth * 0.85, yIn: screenHeight * 0.02)
    }
    
    func pressPause(){
        if(pauseButton.text == "🔊"){
            pauseButton.text = "🔇"
            audioPlayer.pause()
        }else{
            pauseButton.text = "🔊"
            audioPlayer.play()
        }
    }
    
    func setupIcons(){
        
        setupLabel(wallIcon, fontColorIn: UIColor.greenColor(), fontSizeIn: 20, name: "icon", textIn: "〰️", xIn: screenWidth * 0.35, yIn: screenHeight * 0.02)
        setupLabel(wallIconIndicator, fontColorIn: UIColor.greenColor(), fontSizeIn: 20, name: "icon", textIn: "✓", xIn: screenWidth * 0.40, yIn: screenHeight * 0.02)

        setupIcon(magnetIcon, imageSize: CGSize(width: screenWidth * 0.04, height: screenWidth * 0.04),
                  name: "icon", imageName: "Magnet", xIn: screenWidth * 0.47, yIn: screenHeight * 0.03)
        setupLabel(magnetIconIndicator, fontColorIn: UIColor.greenColor(), fontSizeIn: 13, name: "icon", textIn: "❌", xIn: screenWidth * 0.51, yIn: screenHeight * 0.02)

        setupIcon(portalIcon, imageSize: CGSize(width: screenWidth * 0.06, height: screenWidth * 0.03),
                  name: "icon", imageName: "Portal1", xIn: screenWidth * 0.60, yIn: screenHeight * 0.03)
        setupLabel(portalIconIndicator, fontColorIn: UIColor.greenColor(), fontSizeIn: 13, name: "icon", textIn: "❌", xIn: screenWidth * 0.65, yIn: screenHeight * 0.02)
    }
    
    func setupIcon(icon:SKSpriteNode, imageSize:CGSize, name:String, imageName:String, xIn:CGFloat, yIn:CGFloat){
        let positionIn = CGPoint(x: xIn, y: yIn)
        let tex = SKTexture(imageNamed: imageName)
        icon.name = name
        icon.position = positionIn
        icon.texture = tex
        icon.size = imageSize
        self.addChild(icon)
    }
    
    func setupLabels(){
        setupLabel(titleLabel,
                   fontColorIn: UIColor.blackColor(), fontSizeIn: 45, name: "titleLabel", textIn: title,
                   xIn: CGRectGetMidX(self.frame), yIn: CGRectGetMaxY(self.frame)*0.9)
        setupLabel(levelLabel,
                   fontColorIn: UIColor.blackColor(), fontSizeIn: 20, name: "levelLabel",
                   textIn: "Level \(level)", xIn: screenWidth * 0.15, yIn: screenHeight * 0.02)
        setupLabel(toolLimitLabel,
                   fontColorIn: UIColor.blackColor(), fontSizeIn: 20, name: "toolLimitLabel",
                   textIn: "Limit", xIn: screenWidth * 0.73, yIn: screenHeight * 0.017)
    }
    
    func setupLabel(label:SKLabelNode, fontColorIn:UIColor, fontSizeIn:CGFloat, name:String, textIn:String, xIn:CGFloat, yIn:CGFloat){
        let positionIn = CGPoint(x: xIn, y: yIn)
        label.name = name
        label.fontName = labelFont
        label.fontColor = fontColorIn
        label.fontSize = fontSizeIn
        label.text = textIn
        label.position = positionIn
        self.addChild(label)
    }
    
    func setupWalls(){
        //floor
        spawnWall(CGSize(width: screenWidth, height: 1),
                  position: CGPoint(x: CGRectGetMidX(self.frame), y: CGRectGetMinY(self.frame)),
                  name: "wall")
        //ceiling
        spawnWall(CGSize(width: screenWidth, height: 1),
                  position: CGPoint(x: CGRectGetMidX(self.frame), y: CGRectGetMaxY(self.frame)),
                  name: "wall")
        //left wall
        spawnWall(CGSize(width: 1, height: screenHeight),
                  position: CGPoint(x: CGRectGetMinX(self.frame), y: CGRectGetMidY(self.frame)),
                  name: "wall")
        //right wall
        spawnWall(CGSize(width: 1, height: screenHeight),
                  position: CGPoint(x: CGRectGetMaxX(self.frame), y: CGRectGetMidY(self.frame)),
                  name: "wall")
    }
    
    func spawnWall(size:CGSize, position:CGPoint, name:String){
        let wall = SKShapeNode(rectOfSize: size)
        wall.name = name
        wall.fillColor = UIColor.blackColor()
        wall.zPosition = 99
        wall.position = position
        // ROTATE
        wall.strokeColor = UIColor.blackColor()
        wall.physicsBody = SKPhysicsBody(rectangleOfSize: size)
        wall.physicsBody!.affectedByGravity = false
        wall.physicsBody!.dynamic = false
        wall.physicsBody!.categoryBitMask = PhysicsCategory.wall
        wall.physicsBody!.contactTestBitMask = PhysicsCategory.ball
        wall.physicsBody!.collisionBitMask = PhysicsCategory.ball
        wall.physicsBody!.mass = 10
        self.addChild(wall)
    }
    
    func spawnBall(colorIn:UIColor, radius:CGFloat, xIn:CGFloat, yIn:CGFloat){
        ball = SKShapeNode(circleOfRadius: radius)
        ball.name = "ball"
        ballLabel = SKLabelNode(text: String(numBalls));
        numBalls += 1
        ballLabel.fontName = "Arial Black"
        ballLabel.color = UIColor.whiteColor()
        ballLabel.fontSize = 20
        ballLabel.position = CGPoint(x: 0, y: -ball.frame.size.height / 4)
        ballLabel.fontColor = UIColor.clearColor() // ballLabel DISABLED
        ballLabel.name = "ballLabel"
        ball.addChild(ballLabel)
        let positionIn = CGPoint(x: xIn, y: yIn)
        ball.fillColor = colorIn
        ball.strokeColor = UIColor.blackColor()
        ball.zPosition = 99
        ball.physicsBody = SKPhysicsBody(circleOfRadius: radius)
        ball.physicsBody!.affectedByGravity = true
        ball.physicsBody!.dynamic = false
        ball.physicsBody!.categoryBitMask = PhysicsCategory.ball
        ball.physicsBody!.contactTestBitMask = PhysicsCategory.wall | PhysicsCategory.ball
        ball.physicsBody!.collisionBitMask = PhysicsCategory.wall | PhysicsCategory.ball
        ball.physicsBody!.friction = 0.5
        ball.physicsBody!.restitution = 0.8
        ball.physicsBody!.mass = 0.5
        ball.physicsBody!.allowsRotation = true
        ball.physicsBody?.resting = true
        ball.position = positionIn
        self.addChild(ball)
    }
    
    func spawnGoal(radius:CGFloat, xIn:CGFloat, yIn:CGFloat){
        goal = SKShapeNode(circleOfRadius: radius)
        goal.name = "goal"
        goal.position = CGPoint(x: xIn, y: yIn)
        goal.strokeColor = UIColor.blackColor()
        goal.fillColor = UIColor.blueColor()
        goal.physicsBody = SKPhysicsBody(circleOfRadius: radius)
        goal.physicsBody?.affectedByGravity = false
        goal.physicsBody?.dynamic = false
        goal.physicsBody?.categoryBitMask = PhysicsCategory.goal
        goal.physicsBody?.contactTestBitMask = PhysicsCategory.ball
        goal.physicsBody?.collisionBitMask = 0
        let goalPullForce = SKFieldNode.radialGravityField()
        let goalPullForceRadius = goalRadius * 5
        goalPullForce.region = SKRegion(radius: Float(goalPullForceRadius))
        goalPullForce.enabled = true
        goalPullForce.strength = 2
        goalPullForce.position = goal.position
        goalPullForce.name = "goalPull"
        self.addChild(goalPullForce)
        self.addChild(goal)
    }
    
    func spawnMagnet(){
        let frontPoint = findMidPoint(lastWallPoints.first!, point2: lastWallPoints.last!)
        let backPoint = lastWallPoints[lastWallPoints.endIndex / 2]
        let position = findMidPoint(frontPoint, point2: backPoint)
        
        let tex:SKTexture = SKTexture(imageNamed: "Magnet");
        let texSize:CGSize = CGSize(width: ballRadius * 2, height: ballRadius * 2);
        let magnet = SKSpriteNode(texture: tex, size: texSize)
        
        magnet.position = position
        let slope1:CGFloat = (frontPoint.y - backPoint.y) / (frontPoint.x - backPoint.x)
        let slope2:CGFloat = 100000.0
        var radians = atan((slope1 - slope2) / (1+(slope1*slope2)))
        
        if(frontPoint.y < backPoint.y){
            radians += CGFloat(M_PI)
        }
        
        magnet.zRotation = CGFloat(M_PI / 8) // facing up
        magnet.zRotation += radians
        magnet.name = "Magnet"
        magnet.physicsBody = SKPhysicsBody(texture: tex, size: texSize)
        magnet.physicsBody?.affectedByGravity = false
        magnet.physicsBody?.dynamic = false
        magnet.physicsBody?.categoryBitMask = PhysicsCategory.magnet
        //magnet.physicsBody?.contactTestBitMask = PhysicsCategory.ball
        magnet.physicsBody?.collisionBitMask = PhysicsCategory.ball
        let magnetForce = SKFieldNode.radialGravityField()
        let magnetForceRadius = magnet.size.width * 2
        //let circle = SKShapeNode(rect: CGRect(origin: magnet.position, size: CGSize(width: magnet.size.width, height: magnet.size.width)))
        let circle = SKShapeNode(circleOfRadius: magnetForceRadius)
        circle.zRotation = radians
        circle.strokeColor = UIColor.clearColor()
        //circle.fillColor = UIColor.lightGrayColor()
        circle.position = magnet.position
        
        circle.name = "MagnetForceIndicator"
        self.addChild(circle)
        magnetForce.region = SKRegion(radius: Float(magnetForceRadius))
        magnetForce.physicsBody = SKPhysicsBody(circleOfRadius: magnetForceRadius)
        magnetForce.physicsBody?.categoryBitMask = PhysicsCategory.magnetForce
        magnetForce.physicsBody?.contactTestBitMask = PhysicsCategory.ball
        magnetForce.physicsBody?.affectedByGravity = false
        magnetForce.physicsBody?.dynamic = false
        magnetForce.enabled = true
        magnetForce.strength = -30
        magnetForce.position = magnet.position
        magnetForce.name = "MagnetForce"
        currentTools.append(magnet)
        currentTools.append(magnetForce)
        self.addChild(magnetForce)
        self.addChild(magnet)
    }
    
    func spawnPortal() -> Int{
        let position = findMidPoint(lastWallPoints.first!,
                                    point2: lastWallPoints[lastWallPoints.endIndex / 2])
        
        let tex:SKTexture = SKTexture(imageNamed: "Portal\(portalNum)");
        let texSize:CGSize = CGSize(width: ballRadius * 3, height: ballRadius * 1.5);
        
        let tempPortal = SKSpriteNode(texture: tex, size: texSize)
        
        tempPortal.name = "Portal\(portalNum)"
        tempPortal.position = position
        tempPortal.physicsBody = SKPhysicsBody(rectangleOfSize: texSize)
        tempPortal.physicsBody?.affectedByGravity = false
        tempPortal.physicsBody?.dynamic = false
        tempPortal.physicsBody?.categoryBitMask = PhysicsCategory.portal
        tempPortal.physicsBody?.contactTestBitMask = PhysicsCategory.ball
        tempPortal.physicsBody?.collisionBitMask = 0
        
        if(portalNum == 1){
            if(portal1.parent != nil){
                var toolIndex = 0
                while(toolIndex < currentTools.endIndex){
                    if(currentTools[toolIndex].name == "Portal1" ||
                        currentTools[toolIndex].name == "Portal2"){
                        currentTools[toolIndex].removeFromParent()
                    }
                    toolIndex += 1
                }
            }
            portal1 = tempPortal
            portalNum = 2
            currentTools.append(portal1)
            self.addChild(portal1)
            return 1
        }else{
            portal2 = tempPortal
            portalNum = 1
            currentTools.append(portal2)
            self.addChild(portal2)
            return 2
        }
    }
    
    func levelCompleted(){
        playSound("levelCompleted.mp3")
        level += 1
        removeAllTools()
        removeCurrentLevel()
        createLevelFromNum(level)
    }
    
    func playSound(name:String){
        let theSound:SKAction = SKAction.playSoundFileNamed(name, waitForCompletion: false)
        self.runAction(theSound)
    }
    
    func removeCurrentLevel(){
        for node in self.children{
            if(["ball", "goal", "goalPull", "obstacle"].contains(node.name!)){
                node.removeFromParent()
            }
        }
    }
    
    func removeAllTools(){
        for node in self.children{
            if(["Magnet", "MagnetForce", "MagnetForceIndicator", "Portal1", "Portal2", "userWall"].contains(node.name!)){
                node.removeFromParent()
            }
        }
        portalLock = 0
        portalNum = 1
        currentTools.removeAll()
        toolLimitLabel.text = String(limit)
    }
    
//    func playSoundEndlessly(name:String){
//        runAction(SKAction.repeatActionForever(SKAction.sequence([
//            SKAction.waitForDuration(3.0),
//            SKAction.runBlock({self.playSound(name)})])))
//            //SKAction.waitForDuration(144.0)])))
//    }
    
    func playSongAVPlayer(soundName: String)
    {
        let sound = NSURL(fileURLWithPath: NSBundle.mainBundle().pathForResource(soundName, ofType: "mp3")!)
        do{
            audioPlayer = try AVAudioPlayer(contentsOfURL:sound)
            audioPlayer.numberOfLoops = 999
            audioPlayer.prepareToPlay()
            runAction(SKAction.sequence([
                SKAction.waitForDuration(1.5),
                SKAction.runBlock({
                    self.audioPlayer.play()
                    if(self.musicStartsPaused){
                        self.pressPause()
                    }
                })
            ]))
        }catch {
            print("Error getting the audio file")
        }
    }
    
    
    func identifyLine() -> Tool?{
        // Check for circle
        var tool:Tool? = nil
        let firstPoint = lastWallPoints.first!
        let lastPoint = lastWallPoints.last!
        let midPoint = findMidPoint(firstPoint, point2: lastPoint)
        let circle = SKShapeNode(circleOfRadius: dist(firstPoint,point2: midPoint))
        circle.position = midPoint
        circle.name = "circle"
        self.addChild(circle)
        if(dist(firstPoint, point2: lastPoint) < ballRadius &&
            lastWallLength > ball.lineLength &&
            portalsAllowed){
                tool = Tool.portal
        }else if(circle.containsPoint(lastWallPoints[lastWallPoints.endIndex / 2]) == false &&
                magnetsAllowed){
                    tool = Tool.magnet
        }else if(wallsAllowed){
            tool = Tool.line
        }
        circle.removeFromParent()
        return tool
        // Check for magnet
    }
    
    
    func didBeginContact(contact: SKPhysicsContact) {
        var ballBody = contact.bodyA
        var otherBody = contact.bodyB
        if(ballBody.categoryBitMask > otherBody.categoryBitMask){
            ballBody = contact.bodyB
            otherBody = contact.bodyA
        }
        if(otherBody.categoryBitMask == PhysicsCategory.wall){
            if(bounceNoise == 0){
                playSound("bounce.wav")
            }else{
                playSound("bounce.mp3")
            }
        }else if(otherBody.categoryBitMask == PhysicsCategory.goal){
            levelCompleted()
        }else if(otherBody.categoryBitMask == PhysicsCategory.portal){
            if(portalLock == 0){
                playSound("Teleport.wav")
                portalLock = 1
                self.runAction(SKAction.sequence([
                    SKAction.waitForDuration(0.5),
                    SKAction.runBlock({
                        self.portalLock = 0
                    })]))
                ball.physicsBody?.velocity.dy = -(ball.physicsBody?.velocity.dy)!
                if(otherBody.node?.name == "Portal1"){
                    if(portal2.parent != nil){
                        ball.runAction(SKAction.moveTo(portal2.position, duration: 0.0))
                    }
                }else{
                    ball.runAction(SKAction.moveTo(portal1.position, duration: 0.0))
                }
            }
        }else if(otherBody.categoryBitMask == PhysicsCategory.magnet){
            
        }else if(otherBody.categoryBitMask == PhysicsCategory.magnetForce){
            if(magnetSoundLock == 0){
                magnetSoundLock = 1
                self.runAction(SKAction.sequence([
                    SKAction.waitForDuration(0.9),
                    SKAction.runBlock({
                        self.magnetSoundLock = 0
                    })]))
                playSound("Magnet3.mp3")
            }
        }
    }
    
    func incrementIntLabel(label:SKLabelNode, incrementBy:Int){
    //Having to convert labels to ints and back to strings is fucking annoying
        label.text = String(Int(label.text!)! + incrementBy)
    }
    
    override func touchesBegan(touches: Set<UITouch>, withEvent event: UIEvent?) {
        //ball.physicsBody?.resting = true
        var functionPerformed = false
        for node in nodesAtPoint((touches.first?.locationInNode(self))!){
            if(node.name == "pauseMusic"){
                pressPause()
                functionPerformed = true
            }else if(node.name == "reset"){
                removeAllTools()
                functionPerformed = true
            }
        }
        if(functionPerformed == false){
            
            if(ball.parent == nil){
                if(numBalls % 2 == 0){
                    ballColor = originalBallColor
                }else{
                    ballColor = otherBallColor
                }
                spawnBall(ballColor, radius: ballRadius, xIn: ballSpawnPosition.x, yIn: ballSpawnPosition.y)
            }
            ball.physicsBody?.dynamic = false
            ball.position = ballSpawnPosition
            lastWall = SKShapeNode()
            lastWallLength = 0.0
            lastWallPoints.removeAll()
        }
    }
    
    override func touchesEnded(touches: Set<UITouch>, withEvent event: UIEvent?) {
        var drawLine = false
        var dropBall = false
        if(lastWallPoints.count > 1){
            let tool = identifyLine()
            if(tool==Tool.magnet){
                lastWall.removeFromParent()
                spawnMagnet()
                dropBall = true
            }else if(tool==Tool.portal){
                lastWall.removeFromParent()
                if(spawnPortal() == 2){
                    dropBall = true
                }
            }else if(tool==Tool.line){
                drawLine = true
                dropBall = true
            }
            if(drawLine == false){
                lastWall.removeFromParent()
                lastWallPoints.removeAll()
                lastWallLength = 0
            }else{
                currentTools.append(lastWall)
            }
            if(dropBall){
                var notReset = true
                ball.physicsBody?.dynamic = true
                var offset:Int
                if(currentTools.last?.name == "Magnet" ||
                    currentTools.last?.name == "MagnetForce"){
                    offset = 3
                }else{
                    offset = 2
                }
                if(Int(toolLimitLabel.text!)! < 1){
                    if(currentTools[currentTools.count-offset].name == "Magnet" ||
                        currentTools[currentTools.count-offset].name == "MagnetForce"){
                        currentTools.removeAtIndex(currentTools.count-offset).removeFromParent()
                    }
                    if(currentTools.last?.name != "Portal2"){
                        currentTools.removeAtIndex(currentTools.count-offset).removeFromParent()
                    }
                }else{
                    for node in nodesAtPoint((touches.first?.locationInNode(self))!){
                        if(node.name == "reset"){
                            notReset = false
                        }
                    }
                    if(notReset){
                        incrementIntLabel(toolLimitLabel, incrementBy: -1)
                    }
                }
            }
        }
        print(lastWallPoints)
    }
    
    override func touchesMoved(touches: Set<UITouch>, withEvent event: UIEvent?) {
        //Draw wall
        //ball.physicsBody?.resting = true
        lastWall.removeFromParent()
        lastWall = SKShapeNode()
        lastWall.name = "userWall"
        let touchLocation = touches.first?.locationInNode(self)
        
        lastWallPoints.append(CGPoint(x: (touchLocation?.x)!, y: (touchLocation?.y)!))
        while(lastWallLength > maxWallLength){
            lastWallLength -= dist(lastWallPoints[0], point2: lastWallPoints[1])
            lastWallPoints.removeFirst()
        }
        
        drawWall()
    }
    
    func makePathFromPointArray(pointArray:[CGPoint]) -> CGPath{
        let path = CGPathCreateMutable()
        CGPathMoveToPoint(path, nil, (pointArray.first?.x)!, (pointArray.first?.y)!)
        if(pointArray.count>1){
            for point in pointArray{
                CGPathAddLineToPoint(path, nil, point.x, point.y)
            }
        }
        return path
    }
    
    func makePointArrayFromTupleArray(tuples:[(CGFloat,CGFloat)]) -> [CGPoint]{
        var pointArray = [CGPoint]()
        for tuple in tuples{
            pointArray.append(CGPoint(x: tuple.0, y: tuple.1))
        }
        return pointArray
    }
    
    func drawWall(){
        let path = CGPathCreateMutable()
        CGPathMoveToPoint(path, nil, (lastWallPoints.first?.x)!, (lastWallPoints.first?.y)!)
        if(lastWallPoints.count>1){
            lastWallLength += dist(lastWallPoints[lastWallPoints.count-2],point2: lastWallPoints[lastWallPoints.count-1])
            for point in lastWallPoints{
                CGPathAddLineToPoint(path, nil, point.x, point.y)
            }
        }
        lastWall.path = path
        lastWall.lineCap = .Round
        lastWall.lineJoin = .Bevel
        lastWall.zPosition = 99
        lastWall.position = position
        lastWall.strokeColor = UIColor.blackColor()
        lastWall.lineWidth = 5
        lastWall.physicsBody = SKPhysicsBody(edgeChainFromPath: path)
        lastWall.physicsBody?.restitution = 0.8
        lastWall.physicsBody?.affectedByGravity = false
        lastWall.physicsBody?.dynamic = false
        lastWall.physicsBody?.categoryBitMask = PhysicsCategory.wall
        lastWall.physicsBody?.contactTestBitMask = PhysicsCategory.ball
        lastWall.physicsBody?.collisionBitMask = PhysicsCategory.ball
        lastWall.physicsBody?.mass = 10
        self.addChild(lastWall)
    }
    
    func dist(point1:CGPoint, point2:CGPoint) -> CGFloat{
        let first = pow((point1.x - point2.x),2.0)
        let second = pow((point1.y - point2.y),2.0)
        return sqrt(first + second)
    }
    
    func findMidPoint(point1:CGPoint, point2:CGPoint) -> CGPoint{
        let x = (point1.x + point2.x) / 2
        let y = (point1.y + point2.y) / 2
        return CGPoint(x: x, y: y)
    }
    
    override func update(currentTime: CFTimeInterval) {
        /* Called before each frame is rendered */
    }
    
    func loadLevelDictionary(){
        
        let url = NSBundle.mainBundle().URLForResource("levels", withExtension: "json")
        let data = NSData(contentsOfURL: url!)
        
        do {
            let object = try NSJSONSerialization.JSONObjectWithData(data!, options: .AllowFragments)
            if let dictionary = object as? [String: AnyObject] {
                levelDictionary = dictionary
            }
        } catch {
            // Handle Error
        }
    }
    
    func restart(){
        level = 1
        createLevelFromNum(level)
    }
    
    func noLevelsLeft(){
        print("Completed all levels!")
        restart() // ?
    }
    
    //func createWallArray()
    
    func tupleToSKShapeNode(tuple:[(CGFloat,CGFloat)]) -> SKShapeNode{
        return SKShapeNode(path: makePathFromPointArray(makePointArrayFromTupleArray(tuple)))
    }
    
    func changeIcons(allowWalls:Bool, allowMagnets:Bool, allowPortals:Bool){
        wallsAllowed = allowWalls
        magnetsAllowed = allowMagnets
        portalsAllowed = allowPortals
        if(allowWalls){
            wallIconIndicator.text = "✓"
            wallIconIndicator.fontSize = 20
        }else{
            wallIconIndicator.text = "❌"
            wallIconIndicator.fontSize = 13
        }
        if(allowMagnets){
            magnetIconIndicator.text = "✓"
            magnetIconIndicator.fontSize = 20
        }else{
            magnetIconIndicator.text = "❌"
            magnetIconIndicator.fontSize = 13
        }
        if(allowPortals){
            portalIconIndicator.text = "✓"
            portalIconIndicator.fontSize = 20
        }else{
            portalIconIndicator.text = "❌"
            portalIconIndicator.fontSize = 13
        }
        
    }
    
    func spawnObstacle(obstacle:SKShapeNode, position:CGPoint, name:String, filled:Bool){
        if(filled){
            obstacle.fillColor = UIColor.blackColor()
        }
        obstacle.lineCap = .Round
        obstacle.lineJoin = .Bevel
        obstacle.zPosition = 99
        obstacle.position = position
        obstacle.strokeColor = UIColor.blackColor()
        obstacle.lineWidth = 5
        obstacle.name = name
        obstacle.physicsBody = SKPhysicsBody(edgeChainFromPath: obstacle.path!)
        obstacle.physicsBody?.restitution = 0.8
        obstacle.physicsBody?.affectedByGravity = false
        obstacle.physicsBody?.dynamic = false
        obstacle.physicsBody?.categoryBitMask = PhysicsCategory.wall
        obstacle.physicsBody?.contactTestBitMask = PhysicsCategory.ball
        obstacle.physicsBody?.collisionBitMask = PhysicsCategory.ball
        obstacle.physicsBody?.mass = 10
        self.addChild(obstacle)
    }
    
    func createLevel(goalX: CGFloat, goalY:CGFloat, ballX:CGFloat, ballY:CGFloat, walls:[(SKShapeNode,CGPoint,Bool)], allowWalls:Bool, allowMagnets:Bool, allowPortals:Bool, limitIn:Int){
        var color:UIColor
        if(level < Int(ceil(CGFloat(totalLevels) / 3.0))){
            color = UIColor.yellowColor()
        }else if(level < Int(ceil(CGFloat(totalLevels) / 2.0))){
            color = UIColor.orangeColor()
        }else{
            color = UIColor.redColor()
        }
        limit = limitIn
        toolLimitLabel.text = String(limitIn)
        
        changeIcons(allowWalls, allowMagnets: allowMagnets, allowPortals: allowPortals)
        
        for wall in walls{
            spawnObstacle(wall.0, position: wall.1, name: "obstacle", filled: wall.2)
        }
        
        spawnBall(color, radius: ballRadius, xIn: ballX * screenWidth, yIn: ballY * screenHeight)
        spawnGoal(goalRadius, xIn: goalX * screenWidth, yIn: goalY * screenHeight)
    }
    
//    func readJSONObject() {
//        guard let level = levelDictionary["dataTitle"] as? String,
//            let version = levelDictionary["swiftVersion"] as? Float,
//            let users = levelDictionary["users"] as? [[String: AnyObject]] else { return }
//        _ = "Swift \(version) " + title
//        
//        for user in users {
//            guard let name = user["name"] as? String,
//                let age = user["age"] as? Int else { break }
//                //print(name + " is \(age) years old.")
//        }
//    }

    func createLevelFromNum(levelNum:Int){
        levelLabel.text = "Level \(levelNum)"
        let walls = createLevelWallArrayFromLevel(levelNum)
        
        switch(levelNum){
        case 1:
            createLevel(0.95, goalY: 0.5, ballX: 0.2, ballY: 0.9, walls: walls, allowWalls: true, allowMagnets: false, allowPortals: false, limitIn: 1)
            break
            
        case 2:
            createLevel(0.95, goalY: 0.5, ballX: 0.2, ballY: 0.9, walls: walls, allowWalls: false, allowMagnets: true, allowPortals: false, limitIn: 1)
            break
            
        case 3:
            createLevel(0.95, goalY: 0.5, ballX: 0.2, ballY: 0.9, walls: walls, allowWalls: false, allowMagnets: false, allowPortals: true, limitIn: 1)
            break
            
        case 4:
            createLevel(0.9, goalY: 0.9, ballX: 0.2, ballY: 0.9, walls: walls, allowWalls: false, allowMagnets: false, allowPortals: true, limitIn: 1)
            break
            
        case 5:
            createLevel(0.9, goalY: 0.9, ballX: 0.2, ballY: 0.9, walls: walls, allowWalls: false, allowMagnets: true, allowPortals: false, limitIn: 1)
            break
        case 6:
            createLevel(0.95, goalY: 0.5, ballX: 0.2, ballY: 0.9, walls: walls, allowWalls: true, allowMagnets: false, allowPortals: false, limitIn: 2)
            break
        default:
            noLevelsLeft() // All completed?
            break
        }
    }

    func createLevelWallArrayFromLevel(levelNum:Int) -> [(SKShapeNode,CGPoint, Bool)]{
        var walls = [(SKShapeNode,CGPoint, Bool)]()
        switch(levelNum){
        case (1...3):
            return walls
        case 4:
            walls.append((
                SKShapeNode(rectOfSize: CGSize(width: ballRadius * 0.5, height: screenHeight * 0.9)),
                CGPoint(x: screenWidth * 0.8, y: screenHeight * 0.5),
                true))
        case 5:
            walls.append((
                SKShapeNode(rectOfSize: CGSize(width: screenWidth * 0.5, height: ballRadius * 0.5)),
                CGPoint(x: screenWidth * 0.8, y: screenHeight * 0.8),
                true))
        case 6:
            let wall1:[(CGFloat,CGFloat)] = [(314.5, 354.5), (295.999969482422, 354.5), (275.999969482422, 354.5), (265.499969482422, 354.5), (243.499969482422, 350.5), (235.999984741211, 348.5), (226.499969482422, 345.0), (223.499984741211, 344.0), (211.999984741211, 336.0), (206.999984741211, 330.5), (202.999969482422, 324.5), (194.499984741211, 311.0), (187.499984741211, 293.0), (180.499969482422, 275.0), (173.999984741211, 257.5), (167.999969482422, 240.5), (165.499969482422, 233.5), (164.499984741211, 225.5), (163.499984741211, 220.5), (161.999984741211, 208.0), (161.999984741211, 204.0), (161.999984741211, 193.500015258789), (161.999984741211, 190.500015258789), (161.999984741211, 187.500015258789), (160.999984741211, 185.5), (160.999984741211, 181.0), (159.999984741211, 178.0), (159.999984741211, 175.000015258789), (159.999984741211, 171.5), (159.999984741211, 171.0), (159.999984741211, 170.0)]
            walls.append((tupleToSKShapeNode(wall1),
                CGPoint(x: screenWidth * 0, y: screenHeight * 0),
                false))
            
        default:
            break
        }
        return walls
    }
}


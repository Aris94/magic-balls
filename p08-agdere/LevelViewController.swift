//
//  LevelViewController.swift
//  p08-agdere
//
//  Created by Aris Agdere on 4/25/16.
//  Copyright © 2016 aagdere1. All rights reserved.
//

//
//  MainMenuViewController.swift
//  p08-agdere
//
//  Created by Aris Agdere on 4/23/16.
//  Copyright © 2016 aagdere1. All rights reserved.
//

import UIKit
import SpriteKit

class LevelViewController: UIViewController {
    
    var screenWidth:CGFloat = 0.0
    var screenHeight:CGFloat = 0.0
    var startGameButton:UIButton = UIButton(type: UIButtonType.RoundedRect)
    var gameViewController = GameViewController()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        assignGlobalVariables()
        setupBackground()
        setupButtons()
    }
    
    func assignGlobalVariables(){
        screenWidth = UIScreen.mainScreen().bounds.width
        screenHeight = UIScreen.mainScreen().bounds.height
    }
    
    func setupButtons(){
        setupButton(startGameButton, text: "START", position: CGPoint(x: screenWidth/2, y: screenHeight/2))
    }
    
    func setupButton(button:UIButton, text:String, position:CGPoint){
        let size = CGSize(width: screenWidth * 0.4, height: screenHeight * 0.1)
        button.setTitle(text, forState: UIControlState.Normal)
        button.titleLabel?.textColor = UIColor.lightGrayColor()
        button.frame = CGRect(origin: position, size: size)
        button.center = position
        button.backgroundColor = UIColor.redColor()
        button.showsTouchWhenHighlighted = true
        button.addTarget(self, action: #selector(LevelViewController.buttonAction(_:)), forControlEvents: UIControlEvents.TouchUpInside)
        self.view.addSubview(button)
    }
    
    func buttonAction(sender: UIButton){
        if(sender.currentTitle == "START"){
            loadGame()
        }
    }
    
    func setupBackground(){
        self.view.backgroundColor = UIColor.lightGrayColor()
    }
    
    func loadGame(){
        self.showViewController(gameViewController, sender: self)
    }
    
    
}